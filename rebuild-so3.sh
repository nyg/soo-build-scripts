#!/usr/bin/env bash

# exit when any command fails
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command finished with exit code $?."' EXIT


root=$(pwd)

cd "$root"/usr
make

cd "$root"/so3
make

cd "$root"
./deploy.sh -bu
