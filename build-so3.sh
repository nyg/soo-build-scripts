#!/usr/bin/env bash

# exit when any command fails
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command finished with exit code $?."' EXIT


root=$(pwd)

cd "$root"/qemu
./configure --target-list=arm-softmmu --disable-attr --disable-werror --disable-docs
make -j8

cd "$root"/u-boot
make vexpress_defconfig
make -j8

cd "$root"/filesystem
./create_img.sh vexpress

cd "$root"/usr
make

cd "$root"/so3
make vexpress_mmc_defconfig
make

cd "$root"
./deploy.sh -bu
