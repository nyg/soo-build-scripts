#!/usr/bin/env bash

# exit when any command fails
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command finished with exit code $?."' EXIT


root=$(pwd)

##
## U-Boot

cd "$root"/u-boot
make vexpress_defconfig
make -j8

##
## QEMU

cd "$root"/qemu
./configure --target-list=arm-softmmu --disable-attr --disable-werror --disable-docs
make -j8

##
## OP-TEE

cd "$root"/optee_os
./build.sh

cd "$root"/optee_ta
./build.sh

cd "$root"/trusted-firmware-a
./build.sh

##
## Agency

cd "$root"/agency/rootfs
make vexpress_defconfig
make source
make BR2_TOOLCHAIN_GCC_AT_LEAST="7"

cd "$root"/agency/usr
./build.sh

cd "$root"/agency/linux
# TODO enable vfb backend by default, vfb should disable FRAMEBUFFER_CONSOLE for all arch
# enable mouse_ps2 && INPUT_MOUSE
make vexpress_defconfig
make -rR --no-print-directory Image -j8
make -rR --no-print-directory dtbs

cd "$root"/agency/avz
make vexpress_tz_defconfig
make -j8

cd "$root"/agency/filesystem
sudo ./create_img.sh vexpress

##
## ME 

cd "$root"/ME/SOO.refso3/usr
make

cd "$root"/ME/SOO.refso3/so3
make so3virt_defconfig
make

cd "$root"/ME/SOO.refso3
./deploy.sh

##
## Deploy

cd "$root"/agency
./deploy.sh -a SOO.refso3
