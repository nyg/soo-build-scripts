#!/usr/bin/env bash

# exit when any command fails
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command finished with exit code $?."' EXIT


root=$(pwd)

##
## Linux

cd "$root"/agency/linux
make -rR --no-print-directory Image -j8
make -rR --no-print-directory dtbs

##
## ME 

cd "$root"/ME/SOO.refso3/usr
make

cd "$root"/ME/SOO.refso3/so3
make

cd "$root"/ME/SOO.refso3
./deploy.sh

##
## Deploy

cd "$root"/agency
./deploy.sh -a SOO.refso3
