#!/usr/bin/env bash

# exit when any command fails
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command finished with exit code $?."' EXIT

root=$(pwd)


# TODO modify agency/build.conf before

## U-Boot

cd "$root"/u-boot
make rpi_4_32b_defconfig
make -j8

## Agency

cd "$root"/agency/rootfs
make rpi4_defconfig # TODO change gcc and linux headers version
make source
make

cd "$root"/agency/usr
./build.sh

cd "$root"/agency/linux
make rpi4_defconfig # TODO removed some wifi drivers, enable proper backends
make -rR --no-print-directory Image -j8
make -rR --no-print-directory dtbs

cd "$root"/agency/avz
make rpi4_defconfig
make -j8

cd "$root"/agency/filesystem
./create_img.sh rpi4

## ME 

cd "$root"/ME/SOO.refso3/usr
make

cd "$root"/ME/SOO.refso3/so3
make so3virt_defconfig # TODO enable proper front-ends and drivers
make

cd "$root"/ME/SOO.refso3
./deploy.sh

## Deploy

cd "$root"/agency
./deploy.sh -a SOO.refso3
